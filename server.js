var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');
var path       = require('path');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, '/public')));

var port = process.env.PORT || 8080;

var router = express.Router();

router.route('/answer0')

  .post(function(req, res){
    console.log(req.body);
    var answer = req.body.data;
    if (answer == 'tgsothrioitutef'){
      res.json({ message: ['Correct!', "Remember the passcode 'euler'. Enter next to access the 2nd task"], answer: 'tgsothrioitutef', state: 1});
    } else {
      res.json({message: ["Wrong answer"], state: 0});
    }
  });

router.route('/answer1')

  .post(function(req, res){
    console.log(req.body);
    var answer = req.body.data;
    console.log(answer);
    if (answer == '271896534'){
      res.json({ message: ['Correct!', "Remember the passcode 'godel'. Enter next to access the 3rd task"], answer: '271896534', state: 2});
    } else {
      res.json({message: ["Wrong answer"], state: 1});
    }
  });

router.route('/answer2')

  .post(function(req, res){
    console.log(req.body);
    var answer = req.body.data;
    console.log(answer);
    if (answer == '10110'){
      res.json({ message: ['Correct!', "Remember the passcode 'galois'", "Make sure to download the android app from the description below when you come to the treasure hunt."], answer: '10110', state: 3});
    } else {
      res.json({message: ["Wrong answer"], state: 2});
    }
  });

router.route('/answerf')

  .post(function(req, res){
    console.log(req.body);
    var answer = req.body.data;
    if (answer == 'fermatknowsbest'){
      res.json({ message: ['',
                           'Congratulations!',
                           '',
                           "You have succesfully discovered all the qr codes.",
                           "Meet one of the organizers and say the phrase 'Fermat knows best' to claim your prize.",
                           "Be quick before someone else get there."]});
    }
  });

router.route('/givememore')

  .post(function(req, res){
    console.log(req.body);
    var answer = req.body.data[0];
    if (answer == 'tgsothrioitutef') {
      res.json({ message: [ "A common security method used for online banking is to ask the user for three random characters from a passcode. "+
                             "For example, if the passcode was 3145962, they may ask for the 2nd, 4th, and 5th characters; "+
                             "the expected reply would be: 159.",
                            "Given that the three characters are always asked for in order and if the following are succesfull login attempts. Find the shortest possible passcode of unknown length",
                            "",
                            "764",
                            "265",
                            "934",
                            "853",
                            "734",
                            "196",
                            "654",
                            "213",
                            "786",
                            "713"
                            ], state: 1 });
    } else {
      res.json({message: ["Wrong answer"], state: 1 });
    }
  });

router.route('/final')

  .post(function(req, res){
    console.log(req.body);
    var answer = req.body.data[0];
    if (answer == '271896534'){
      res.json({ message: [ "input pattern: N 0 1 1 0 1(p) N",
                            "#              //starting point",
                            "if p = 1       //p is the bit pointed by the HEAD",
                            "   -->         //move pointer to right by one cell",
                            "   if p = 0",
                            "      p <= 1   //store 1 at the current position of the HEAD",
                            "   else if p = 1",
                            "      p <= 0",
                            "",
                            "   <--, <-- ",
                            "",
                            "   if p = 0",
                            "      p <= 1",
                            "   else if p = 1",
                            "      p <= 0",
                            "",
                            "   goto #      //goto starting point #",
                            "",
                            "else if p = 0",
                            "   <-- ",
                            "   goto # ",
                            "else",
                            "   halt        //terminates the program",
                            "What is the output of the program?",
                            "eg input: N 0 1 0 1 1 1(p) N",
                            "eg output: 001011"
                            ], state: 2});
    } else {
      res.json({message: ["Wrong answer"], state: 2});
    }
  });

app.use('/api', router);

app.get('/', function(req, res) {
  res.sendfile('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
});

// START THE SERVER
app.listen(port);
console.log('Magic happens on port ' + port);
