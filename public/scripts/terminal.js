﻿angular.module('ieee-app', ['ngRoute', 'vtortola.ng-terminal', 'ng-terminal-example.command.tools', 'ng-terminal-example.command.implementations', 'ng-terminal-example.command.filesystem'])
.provider('$ga', function () {

    window['GoogleAnalyticsObject'] = 'ga';
    window['ga'] = window['ga'] || function () { (window['ga'].q = window['ga'].q || []).push(arguments) }
    window['ga'].l = 1 * new Date();
    var script = document.createElement('script');
    var prevScript = document.getElementsByTagName('script')[0];
    script.async = 1;
    script.src = '//www.google-analytics.com/analytics_debug.js';
    prevScript.parentNode.insertBefore(script, prevScript);

    var provider = function () {
        var me = {};

        me.$get = function () {
            ga('send', 'pageview');
            return function () {
                return window.ga.apply(window, arguments);
            }
        };

        me.ga = function () {
            return window.ga.apply(window, arguments);
        };

        return me;
    };

    return provider();
})
.config(function($routeProvider, $locationProvider) {
  // $locationProvider.html5Mode(true);

  $routeProvider
  .when('/', {
    templateUrl: 'src/partials/home.html',
    controller: 'console'
  })
  .when('/final/:id', {
    templateUrl: 'src/partials/final.html',
    controller: 'finalCtrl'
  });
})
.controller('console',['$scope','$ga','commandBroker','$rootScope','$http', function ($scope, $ga, commandBroker, $rootScope, $http) {

    $scope.state = 0;

    var answer;
    var splash = ['Welcome to IEEE quest v0.0.1',
                   '',
                   'Hello!',
                   "Your first task, should you choose to accept it, is to decrypt the following text",
                   '',
                   "ymjl wjfy jxyx mtwy htrn sltk ymjm zrfs wfhj nxtz wnsf gnqn ydyt zsij wxyf siym jjcu tsjs ynfq kzsh ynts",
                   '',
                   'Enter the first letters of each word of the plaintext to the console',
                   "Ex: If the plaintext is:",
                   "To be or not to be, that is the question",
                   "You should enter 'tbontbtitq' to the console",
                   "",
                   "Hint: Caesar",];

    var logo = ["  _    _  _____  _____  _____   _____ ______ ______ ______   _____      __     __",
                " | |  | |/ ____|/ ____|/ ____| |_   _|  ____|  ____|  ____| |  __ \\   /\\\\ \\   / /",
                " | |  | | |    | (___ | |        | | | |__  | |__  | |__    | |  | | /  \\\\ \\_/ / ",
                " | |  | | |     \\___ \\| |        | | |  __| |  __| |  __|   | |  | |/ /\\ \\\\   /  ",
                " | |__| | |____ ____) | |____   _| |_| |____| |____| |____  | |__| / ____ \\| |   ",
                "  \\____/ \\_____|_____/ \\_____| |_____|______|______|______| |_____/_/    \\_\\_| ",
                '',
                ''];

    setTimeout(function () {
        $scope.$broadcast('terminal-output', {
            output: true,
            text: splash,
            breakLine: true
        });
        $scope.$apply();
    }, 100);

    function isMobile() {
      var check = false;
      (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
      return check;
    }

    if(!isMobile()){
        splash = logo.concat(splash);
    }

    $scope.session = {
        commands: [],
        output: [],
        $scope:$scope
    };

    $scope.$watchCollection(function () { return $scope.session.commands; }, function (n) {
        for (var i = 0; i < n.length; i++) {
            $ga('send', 'event', 'Console', 'Command', JSON.stringify(n[i]));
            $scope.$broadcast('terminal-command', n[i]);
        }
        $scope.session.commands.splice(0, $scope.session.commands.length);
        $scope.$$phase || $scope.$apply();
    });

    $scope.$watchCollection(function () { return $scope.session.output; }, function (n) {
        for (var i = 0; i < n.length; i++) {
            $ga('send', 'event', 'Console', 'Output', JSON.stringify(n[i]));
            $scope.$broadcast('terminal-output', n[i]);
        }
        $scope.session.output.splice(0, $scope.session.output.length);
        $scope.$$phase || $scope.$apply();
    });

    $scope.$on('$viewContentLoaded', function (event) {
        $ga('send', 'pageview');
    });

    $scope.$on('terminal-input', function (e, consoleInput) {
        var cmd = consoleInput[0];
        console.log(cmd.command);
        console.log("State", $scope.state);
        if (cmd.command === 'next') {
            console.log("next challenge", answer);
            if ($scope.state == 1){
                $http.post('/api/givememore', {data:[answer, cmd.command]}).
                  then(function(response) {
                    console.log(response.data.message);
                    $scope.state = response.data.state;
                    $scope.session.output.push({ output: true, breakLine: true, text: response.data.message});
                  }, function(response) {
                    $scope.session.output.push({ output: true, breakLine: true, text: ["Something went wrong. Please try again"] });
                  });
            } else if ($scope.state == 2) {
                $http.post('/api/final', {data:[answer, cmd.command]}).
                  then(function(response) {
                    console.log(response.data.message);
                    $scope.state = response.data.state;
                    $scope.session.output.push({ output: true, breakLine: true, text: response.data.message});
                  }, function(response) {
                    $scope.session.output.push({ output: true, breakLine: true, text: ["Something went wrong. Please try again"] });
                  });
            }
        } else if ($scope.state == 0) {
            $http.post('/api/answer0', {data:cmd.command}).
              then(function(response) {
                console.log(response.data);
                $scope.state = response.data.state;
                if (response.data.answer !== undefined){
                    answer = response.data.answer;
                }
                $scope.session.output.push({ output: true, breakLine: true, text: response.data.message});
              }, function(response) {
                $scope.session.output.push({ output: true, breakLine: true, text: ["Something went wrong. Please try again"] });
              });
        } else if ($scope.state == 1){
            $http.post('/api/answer1', {data:cmd.command}).
              then(function(response) {
                console.log(response.data);
                $scope.state = response.data.state;
                if (response.data.answer !== undefined){
                    answer = response.data.answer;
                }
                $scope.session.output.push({ output: true, breakLine: true, text: response.data.message});
              }, function(response) {
                $scope.session.output.push({ output: true, breakLine: true, text: ["Something went wrong. Please try again"] });
              });
        } else if ($scope.state == 2){
            $http.post('/api/answer2', {data:cmd.command}).
              then(function(response) {
                console.log(response.data);
                $scope.state = response.data.state;
                if (response.data.answer !== undefined){
                    answer = response.data.answer;
                }
                $scope.session.output.push({ output: true, breakLine: true, text: response.data.message});
              }, function(response) {
                $scope.session.output.push({ output: true, breakLine: true, text: ["Something went wrong. Please try again"] });
              });
        }
    });
}])

.controller('finalCtrl',['$scope','$ga','commandBroker','$rootScope','$http', '$routeParams', function ($scope, $ga, commandBroker, $rootScope, $http, $routeParams) {
    console.log("finalCtrl fired", $routeParams.id);
    var id = $routeParams.id;

    $scope.session = {
        commands: [],
        output: [],
        $scope:$scope
    };

    $scope.$watchCollection(function () { return $scope.session.output; }, function (n) {
        for (var i = 0; i < n.length; i++) {
            $ga('send', 'event', 'Console', 'Output', JSON.stringify(n[i]));
            $scope.$broadcast('terminal-output', n[i]);
        }
        $scope.session.output.splice(0, $scope.session.output.length);
        $scope.$$phase || $scope.$apply();
    });

    $scope.$on('$viewContentLoaded', function (event) {
        $ga('send', 'pageview');
    });

    $http.post('/api/answerf', {data:id}).
      then(function(response) {
        console.log(response.data);
        $scope.session.output.push({ output: true, breakLine: true, text: response.data.message});
      }, function(response) {
        $scope.session.output.push({ output: true, breakLine: true, text: ["Something went wrong. Please try again"] });
      });
}])

.config(['$gaProvider',function ($gaProvider) {
    $gaProvider.ga('create', 'UA-68402428-1', 'auto');
}])

.config(['terminalConfigurationProvider', function (terminalConfigurationProvider) {

    terminalConfigurationProvider.config('vintage').outputDelay = 10;
    terminalConfigurationProvider.config('vintage').allowTypingWriteDisplaying = false;
    terminalConfigurationProvider.config('vintage').typeSoundUrl ='scripts/content/type.wav';
    terminalConfigurationProvider.config('vintage').startSoundUrl ='scripts/content/start.wav';
}]);

